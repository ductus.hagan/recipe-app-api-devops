.PHONY: build-image compose-proxy

build-image:
	docker build .

compose-proxy:
	docker-compose -f docker-compose-proxy.yml up
